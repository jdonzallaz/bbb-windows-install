# Install BBB development environment on Windows

Small documentation explaining how to install a development environment for the Beaglebone Black on Windows. Works well on Windows 10. Other versions may need adaptations. It will install all the necessary tools to compile, launch, debug and control your program. Eclipse stays necessary to launch the program, because the J-Link plugin is only available for this IDE. Then, you're free to use the IDE of your choice for editing the code.

**Author:** Jonathan Donzallaz  
**With the help of:** Jacques Supcik  
**Tested and reviewed by:** Yann-Ivain Beffa  
**Created:** 09.06.2018  
**Last update:** 13.06.2018

---

Used tools:
- Compilation: Cygwin, make
- Control/BBB command line: PuTTy
- Launch/debug: Eclipse

## Requirements

Download the following software in their last version (for Windows 64bits when available):
- [GCC](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads): GNU Embedded Toolchain for Arm (for windows 32).
- [Jlink](https://www.segger.com/downloads/jlink/#J-LinkSoftwareAndDocumentationPack): J-Link Software and Documentation pack for Windows.
- [Cygwin]( https://cygwin.com/install.html)
- [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html)
- [Eclipse](https://www.eclipse.org/downloads/download.php?file=/oomph/epp/oxygen/R2/eclipse-inst-win64.exe)

## Installation

Install every software downloaded previously. Select default options except for the following:

### GCC

Make sure to install it in a path without space. `C:/Program Files (x86)/` is not valid. Something like `c:/gcc-arm/` is a good solution. Remove the pre-generated subfolder or modify it to remove the spaces (see next figure).

An installation in a bad folder can generate problems. If this happens, uninstall it, remove any values left in the `$PATH` or other environment variables, restart and reinstall it.

This can be helpful in case of problems:
- Check if cygwin find the compiler:
```bash
where arm-none-eabi-gcc
``` 
- Check if the `$PATH` contains an entry for gcc:
```bash
echo $PATH | tr : \\n | grep gcc
``` 
- Remove a bad entry (replace `/gcc-arm/` by the faulty folder):
```bash
export PATH=`echo ${PATH} | awk -v RS=: -v ORS=: '/gcc-arm/ {next} {print}'`
```
- Add the correct entry in the `$PATH`: (to adapt with your path)
```bash
export PATH=$PATH:/cygdrive/c/gcc-arm/bin
```


![alt text][gcc-install]

### Cygwin

When installing Cygwin, you will be asked to choose which package to include. Select the following packages (under develop or debug category):
- gcc-core: C compiler subpackage
- libgcc1: C runtime library
- gdb: The GNU Debugger
- make: The GNU version of the ‘make’ utility
- vim: a basic text editor

After the installation of Cygwin, open the cygwin terminal. Open the `.bash_profile` file. It is located in your home, and you can open it with vim:
```bash
cd ~
vim .bash_profile
```

Add the following command at the end of the file (type `i` to start insert mode, `esc` to quit insert mode, and 2x `shift+z` to quit):
```bash
export LMIBASE=path-to-project-root
```

Example:
```bash
export LMIBASE=C:/code/se12-1718-tp
```

It sets the variable when the bash shell starts. The variable is necessary to compile the project.

![alt text][lmibase]

### Eclipse

When installing Eclipse, choose "Eclipse IDE for C/C++ Developers".
After the installation finishes, open it and go to `Help -> Eclipse Marketplace...`, and search for "Jlink". Select and install "GNU MCU Eclipse 4.3.3" (or latest version).
When asked, choose only the following features:
- ARM Cross Compiler
- RISC-V Cross Compiler
- J-Link debugging

![alt text][eclipse-plugin]

You can then add your project to eclipse (assuming you have already cloned it with git). Select `File -> open project from file system`.

In run configurations, double-click on "GDB SEGGER J-Link Debugging". Select the new entry. Give a name to the configuration, and follow exactly the following instructions and images:

In the "Main" tab, set the name of the project, and search the `app_a` file in the file explorer for the "C/C++ Application" (generated the first time you compile the program with `make`).

![alt text][eclipse-run-main]

In the "Debugger" tab:
- J-Link GDB Server Setup: if the actual executable does not show up with the variables `${jlink_path}/${jlink_gdb_server}`, search it manually in the explorer. It should be something like `C:\Program Files (x86)\SEGGER\JLink_V632e/JLinkGDBServerCL.exe`.
- Add the device name: `am3358`
- Change interface to: `JTAG`
- GDB Client Setup: again, if the actual executable does not show up with the variables `${cross_prefix}gdb${cross_suffix}`, add it manually. It should be something like `arm-none-eabi-gdb`.
If later you have errors related to `arm-none-eabi-gdb ` not found, come back and click on "browse", and search the executable in the installation folder of gcc. It should be something like `C:\gcc-arm\bin\arm-none-eabi-gdb.exe`.

![alt text][eclipse-run-debugger]

In the "Startup" tab:
- Disable "Initial Reset and Halt"
- Change JTAG/SWD Speed to: Fixed, 1000kHz
- Disable "flash breakpoints"
- Disable "semihosting"
- Add the following initialization commands:  
`monitor reset`  
`monitor go`  
`monitor sleep 100`  
`monitor halt`  
- Disable pre-run/restart reset

![alt text][eclipse-run-startup]

Additionally, you can add this configuration to the favorite menu in the "Common" tab.

# Compilation

Open the Cygwin terminal, and go to your project directory, in your current (sub)project directory. Then type `make` to launch compilation. Example:
```bash
cd C:/code/se12-1718-tp/tp.10
make
```

The first time, it should be necessary to compile the bbb library:
```bash
cd C:/code/se12-1718-tp/bbb/source
make
```

In case of problems, it is always advised to clean and recompile the project with:
```
make clean all
```

![alt text][make-command]

# Launch project

## Starting the program

In Eclipse, start the program by selecting the run configuration previously configured.

## Console with Putty

If you want access to the console, like on Linux with `minicom`, you need to use Putty (or equivalent). Open Putty.

Change the connection type to "Serial". With the BBB connected to your computer, open the Windows Device Manager and find which serial port the BBB use.

![alt text][serial-com]

Report it in Putty in "Serial line". Change speed to 115200.

![alt text][putty]

You can save this configuration by adding a name in "Saved Sessions" and clicking "Save". Then, click "Open" to connect to the BBB. Normally, the window will be empty, and will receive data when starting the program.

![alt text][putty-minicom]

[eclipse-plugin]: img/eclipse-plugin.png "Features to choose for J-Link eclipse plugin"
[lmibase]: img/lmibase.png "Set LMIBASE variable in .bash_profile"
[make-command]: img/make-command.png "Compile the program with make command"
[putty-minicom]: img/putty-minicom.png "Get console from the BBB"
[serial-com]: img/serial-com.png "Find the right serial port"
[putty]: img/putty.png "Putty configuration"
[eclipse-run-main]: img/eclipse-run-main.png "Eclipse run configuration: main tab"
[eclipse-run-debugger]: img/eclipse-run-debugger.png "Eclipse run configuration: debugger tab"
[eclipse-run-startup]: img/eclipse-run-startup.png "Eclipse run configuration: startup tab"
[gcc-install]: img/gcc-install.png "GCC installation directory"